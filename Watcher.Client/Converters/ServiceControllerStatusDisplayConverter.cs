﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;
using Watcher.Client.Helpers;

namespace Watcher.Client.Converters
{
    public class ServiceControllerStatusDisplayConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (!(value is ServiceControllerStatus)) throw new ArgumentException("Value is not ServiceControllerStatus");

            var status = (ServiceControllerStatus)value;

            var border = new Border { Padding = new Thickness(5d), CornerRadius = new CornerRadius(3d) };

            var textBlock = new TextBlock { Text = status.GetDisplayName(), Foreground = Brushes.White };

            border.Child = textBlock;

            // Select border color

            switch (status)
            {
                case ServiceControllerStatus.Running:
                    border.Background = Brushes.ForestGreen;
                    break;
                case ServiceControllerStatus.Stopped:
                    border.Background = Brushes.Firebrick;
                    break;
                default:
                    border.Background = Brushes.Orange;
                    break;
            }

            return border;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
