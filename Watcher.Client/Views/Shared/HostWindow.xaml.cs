﻿using System.Windows;

namespace Watcher.Client.Views.Shared
{
    /// <summary>
    /// Interaction logic for HostWindow.xaml
    /// </summary>
    public partial class HostWindow : Window
    {
        public HostWindow()
        {
            InitializeComponent();
        }
    }
}
