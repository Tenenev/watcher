﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Watcher.Client.Common;
using Watcher.Client.Helpers;
using Watcher.Client.ViewModels.Services;
using Watcher.Core.Helpers;

namespace Watcher.Client.ViewModels
{
    public class GlobalViewModel : ViewModelBase
    {
        public ObservableCollection<ServiceViewModel> WatchedServices { get; private set; }

        #region Selected Watched Service

        private ServiceViewModel _selectedWatchedService;

        public ServiceViewModel SelectedWatchedService
        {
            get { return _selectedWatchedService; }
            set
            {
                CanMoveToUnwatched = value != null;
                _selectedWatchedService = value;
                OnPropertyChanged("SelectedWatchedService");
            }
        }

        #endregion

        public ObservableCollection<ServiceViewModel> UnwatchedServices { get; private set; }

        #region Selected Unwatched Service

        private ServiceViewModel _selectedUnwatchedService;

        public ServiceViewModel SelectedUnwatchedService
        {
            get { return _selectedUnwatchedService; }
            set
            {
                CanMoveToWatched = value != null;
                _selectedUnwatchedService = value;
                OnPropertyChanged("SelectedUnwatchedService");
            }
        }

        #endregion

        public GlobalViewModel()
        {
            WatchedServices = new ObservableCollection<ServiceViewModel>();
            UnwatchedServices = new ObservableCollection<ServiceViewModel>();

            InitializeServices();
        }

        private async void InitializeServices()
        {
            var initializeServicesTask = new Task(() =>
            {
                var watchedServices = XmlStore<List<ServiceViewModel>>.Load().Result ?? new List<ServiceViewModel>();

                foreach (var service in ServiceController.GetServices().OrderBy(s => s.DisplayName))
                {
                    var serviceModel = new ServiceViewModel {Name = service.DisplayName, ServiceName = service.ServiceName, Status = service.Status};

                    if(watchedServices.Any(s => s.ServiceName == service.ServiceName))
                        WatchedServices.Add(serviceModel);
                    else
                        UnwatchedServices.Add(serviceModel);
                }
            });

            initializeServicesTask.Start();
            await initializeServicesTask;

            Action<Task> repeatAction = null;

            repeatAction = async task =>
            {
                await UpdateServices();

                await Task.Delay(3000).ContinueWith(repeatAction);
            };

            Task.Delay(3000).ContinueWith(repeatAction);
        }

        private async Task UpdateServices()
        {
            var updateStatusesTask = new Task(() =>
            {
                var services = ServiceController.GetServices().OrderBy(s => s.DisplayName).ToList();

                // Remove not found services
                var oldServices = UnwatchedServices.ToList();
                oldServices.AddRange(WatchedServices);

                // Remove deleted services
                foreach (var oldService in oldServices)
                {
                    if (services.All(s => s.ServiceName != oldService.ServiceName))
                    {
                        if (UnwatchedServices.Contains(oldService))
                            UnwatchedServices.Remove(oldService);
                        if (WatchedServices.Contains(oldService))
                            WatchedServices.Remove(oldService);
                    }
                }

                // Add new services
                foreach (var service in services)
                {
                    // Update status
                    if (WatchedServices.Any(s => s.ServiceName == service.ServiceName))
                    {
                        var existedService = WatchedServices.First(s => s.ServiceName == service.ServiceName);
                        existedService.Status = service.Status;
                        continue;
                    }

                    if (UnwatchedServices.All(s => s.ServiceName != service.ServiceName))
                    {
                        var serviceModel = new ServiceViewModel { Name = service.DisplayName, ServiceName = service.ServiceName, Status = service.Status };
                        UnwatchedServices.Add(serviceModel);
                    }
                    else
                    {
                        var existedService = UnwatchedServices.First(s => s.ServiceName == service.ServiceName);
                        existedService.Status = service.Status;
                    }
                }
            });
            updateStatusesTask.Start();
            await updateStatusesTask;
        }

        private async Task SaveWatchedList()
        {
            await XmlStore<List<ServiceViewModel>>.Save(WatchedServices.ToList());
        }

        #region Commands

        #region Move to Watched

        private RelayCommand _moveToWatchedCommand;

        public ICommand MoveToWatchedCommand
        {
            get
            {
                return _moveToWatchedCommand ??
                       (_moveToWatchedCommand = new RelayCommand(MoveToWatched, () => CanMoveToWatched));
            }
        }

        private bool _canMoveToWatched = false;

        public bool CanMoveToWatched
        {
            get { return _canMoveToWatched; }
            set
            {
                _canMoveToWatched = value;
                if(_moveToWatchedCommand != null)
                    _moveToWatchedCommand.RaiseCanExecuteChanged();
            }
        }

        private void MoveToWatched()
        {
            var selectedService = SelectedUnwatchedService;
            UnwatchedServices.Remove(selectedService);

            var watchedServicesCopy = WatchedServices.ToList();
            watchedServicesCopy.Add(selectedService);
            var index = watchedServicesCopy.OrderBy(s => s.Name).ToList().IndexOf(selectedService);
            WatchedServices.Insert(index, selectedService);

            SaveWatchedList();
        }

        #endregion

        #region Move to Unwatched

        private RelayCommand _moveToUnwatchedCommand;

        public ICommand MoveToUnwatchedCommand
        {
            get
            {
                return _moveToUnwatchedCommand ??
                       (_moveToUnwatchedCommand = new RelayCommand(MoveToUnwatched, () => CanMoveToUnwatched));
            }
        }

        private bool _canMoveToUnwatched = false;

        public bool CanMoveToUnwatched
        {
            get { return _canMoveToUnwatched; }
            set
            {
                _canMoveToUnwatched = value;
                if (_moveToUnwatchedCommand != null)
                    _moveToUnwatchedCommand.RaiseCanExecuteChanged();
            }
        }

        private void MoveToUnwatched()
        {
            var selectedService = SelectedWatchedService;
            WatchedServices.Remove(selectedService);

            var unwatchedServicesCopy = UnwatchedServices.ToList();
            unwatchedServicesCopy.Add(selectedService);
            var index = unwatchedServicesCopy.OrderBy(s => s.Name).ToList().IndexOf(selectedService);
            UnwatchedServices.Insert(index, selectedService);

            SaveWatchedList();
        }

        #endregion

        #endregion
    }
}
