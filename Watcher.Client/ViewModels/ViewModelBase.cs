﻿using System;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Watcher.Client.ViewModels
{
    public abstract class ViewModelBase : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
#if DEBUG
            if (GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance).All(p => p.Name != name))
                throw new Exception(string.Format("Property '{0}' does not exist)", name));
#endif
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(name));
        }

        #endregion
    }
}