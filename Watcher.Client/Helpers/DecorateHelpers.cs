﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Watcher.Client.Helpers
{
    public static class DecorateHelpers
    {
        public static string GetDisplayName(this Enum value)
        {
            var valueStr = value.ToString();

            var sb = new StringBuilder();

            for (var i = 0; i < valueStr.Length; i++)
            {
                if (i != 0 && Char.IsUpper(valueStr[i]))
                    sb.Append(' ');
                sb.Append(valueStr[i]);
            }

            return value.ToString();
        }
    }
}
