﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Watcher.Client.ViewModels;

namespace Watcher.Client
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private static GlobalViewModel _viewModel;

        public static GlobalViewModel ViewModel
        {
            get { return _viewModel; }
        }

        static App()
        {
            _viewModel = new GlobalViewModel();
        }
    }
}
