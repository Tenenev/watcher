﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Watcher.Service
{
    [ServiceContract]
    public interface IWatcherService
    {
        [OperationContract(IsOneWay = true)]
        void UpdateConfig();

        [OperationContract(IsOneWay = true)]
        void StartService(string serviceName);

        [OperationContract(IsOneWay = true)]
        void StopService(string serviceName);
    }
}
